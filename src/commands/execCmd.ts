import { exec, ExecOptions } from 'child_process';

export const execCmd = async (command: string, flags: ExecOptions | null = null) => {
    return new Promise((resolve, reject) => {
        exec(command, flags, (error, output) => {
            if (error) {
                reject(error);
            } else {
                resolve(output);
            }
        });
    });
};
