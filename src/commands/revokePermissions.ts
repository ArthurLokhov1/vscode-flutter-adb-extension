import * as utils from './utils';
import * as vscode from 'vscode';
import { execCmd } from './execCmd';

export const revokePermissions = vscode.commands.registerCommand('flutter-adb.adbRevokePermissions', () => {
    utils.applicationId.then((appid) => {
        utils.chooseDeviceToRun().then((target) => {
            const output = execCmd(`adb -s ${target} shell dumpsys package ${appid}`);
            output.then(data => {
                if (typeof data === 'string') {
                    let grantedPermissions = data
                        .split('\n')
                        .filter((line) => line.indexOf('permission') >= 0 && line.indexOf('granted=true') >= 0)
                        .map((line) => line.split(':')[0].trim());
                }

            });
        });
    });
    console.log('Revoke Permissions: Complete');
    vscode.window.showInformationMessage('Revoke Permissions: Complete');
});
