import * as utils from './utils';
import * as vscode from 'vscode';
import { execCmd } from './execCmd';

export const openApplication = vscode.commands.registerCommand('flutter-adb.adbOpenApplication', () => {
    utils.applicationId.then((appid) => {
        utils.chooseDeviceToRun().then((target) => {
            execCmd(`adb -s ${target} shell monkey -p ${appid} -c android.intent.category.LAUNCHER 1`);
        });
    });
    console.log('Open Application: Complete');
    vscode.window.showInformationMessage('Open Application: Complete');
});
