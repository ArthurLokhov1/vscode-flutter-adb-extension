import * as utils from './utils';
import * as vscode from 'vscode';

export const chooseDeviceToRun = vscode.commands.registerCommand('flutter-adb.adbChooseDeviceToRun', () => {
    utils.chooseDeviceToRun().then(data => console.log(data));
    console.log('Choose Device To Run: Complete');
    vscode.window.showInformationMessage('Choose Device To Run: Complete');
});
