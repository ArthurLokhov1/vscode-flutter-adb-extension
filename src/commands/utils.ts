import * as vscode from 'vscode';
import { promisify } from 'util';
import * as fs from 'fs';
import path = require('path');
import { execCmd } from './execCmd';

const findAndroidApplicationId = async (folders: Array<vscode.Uri>): Promise<String> => {
    const gradleFiles = [
        'app/build.gradle',
        'android/app/build.gradle',
    ]

    // Говорю, что будем проверять только файлы
    const needCheck = folders?.filter((uri) => uri.scheme === 'file');
    console.log(needCheck);
    const file = promisify(fs.readFile);

    for (let folder of needCheck) {
        for (let gradleFile of gradleFiles) {
            let gradleFilePath = path.join(folder.fsPath, gradleFile);
            try {
                let openFile = await file(gradleFilePath, { encoding: "utf8" });
                let lines = openFile.split('\n')
                    .filter((line) => line.indexOf('applicationId') >= 0)
                    .map((line) => {
                        const regexMatch = line.match(/"([^"]*)"/);
                        if (regexMatch === null || regexMatch.length === 0) return '';
                        return regexMatch[0];
                    })
                    .filter((appid) => appid.length > 0);
                if (lines.length > 0) return lines[0];
            }
            catch (err) {
                console.log(`Cannot open file ${gradleFilePath}`);
                continue;
            }
        }
    }

    return '';
};

export const chooseDeviceToRun = async (): Promise<String> => {
    let connectedDevices = (await execCmd('adb devices') as string)
        .split('\n')
        .filter((_, index) => index > 0)
        .map((line) => line.split('\t')[0].trim())
        .filter((line) => line.length > 0);
    if (connectedDevices.length === 0) return '';
    if (connectedDevices.length === 1) return connectedDevices[0];

    let selectedDevice = await vscode.window.showQuickPick(
        connectedDevices,
        {
            title: "Choose target device",
            canPickMany: false,
        }
    );

    return selectedDevice ?? '';
};

const workspaceFolders: Array<vscode.Uri> = [vscode.Uri.file(vscode.workspace.rootPath || '')];
export const applicationId = findAndroidApplicationId(workspaceFolders);
