import * as vscode from 'vscode';
import { execCmd } from './execCmd';
import * as utils from './utils'

export const stopApplication = vscode.commands.registerCommand('flutter-adb.adbStopApplication', () => {
    utils.applicationId.then((appid) => {
        utils.chooseDeviceToRun().then((target) => {
            execCmd(`adb -s ${target} shell am force-stop ${appid}`);
        });
    });
    console.log('Stop Application: Completed');
    vscode.window.showInformationMessage('Stop Application: Completed');

});
