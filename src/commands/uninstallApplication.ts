import * as utils from './utils';
import * as vscode from 'vscode';
import { execCmd } from './execCmd';

export const uninstallApplication = vscode.commands.registerCommand('flutter-adb.adbUninstallApplication', () => {
    utils.applicationId.then((appid) => {
        utils.chooseDeviceToRun().then((target) => {
            execCmd(`adb -s ${target} uninstall ${appid}`);
        });

    });
    console.log('Uninstall Application: Complete');
    vscode.window.showInformationMessage('Uninstall Application: Complete');
});
