import { stopApplication } from './stopApplication';
import { clearAppData } from './clearAppData';
import { openApplication } from './openApplication';
import { uninstallApplication } from './uninstallApplication';
import { revokePermissions } from './revokePermissions';
import { chooseDeviceToRun } from './chooseDeviceToRun';

export {
    stopApplication,
    clearAppData,
    openApplication,
    uninstallApplication,
    revokePermissions,
    chooseDeviceToRun
}
