import * as utils from './utils';
import * as vscode from 'vscode';
import { execCmd } from './execCmd';

export const clearAppData = vscode.commands.registerCommand('flutter-adb.adbClearApplicationData', () => {
    utils.applicationId.then((appid) => {
        utils.chooseDeviceToRun().then((target) => {
            execCmd(`adb -s ${target} shell pm clear ${appid}`);
        });
    });
    console.log('Clear Application Data: Complete');
    vscode.window.showInformationMessage('Clear Application Data: Complete');
});
