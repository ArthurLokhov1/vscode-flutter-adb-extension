import * as vscode from 'vscode';
import * as cmd from './commands/commands';

export function activate(context: vscode.ExtensionContext) {

	console.log('Congratulations, "flutter-adb" is now active!');

	// Register commands
	context.subscriptions.push(cmd.openApplication);
	context.subscriptions.push(cmd.stopApplication);
	context.subscriptions.push(cmd.uninstallApplication);
	context.subscriptions.push(cmd.clearAppData);
	context.subscriptions.push(cmd.revokePermissions);
	context.subscriptions.push(cmd.chooseDeviceToRun);
}



export function deactivate() { }
